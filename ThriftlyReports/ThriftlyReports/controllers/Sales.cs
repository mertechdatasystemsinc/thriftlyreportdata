﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ThriftlyReports.controllers
{
    public struct salesRecord
    {
        public string SalesOrderNumber;
        public string OrderDate;
		public string CompanyName;
		public string Address;
		public string City;
		public string State;
		public int OrderQty;
		public string SalesRep;
		public double LineTotal;
		public string ProductName;
		public double Cost;
		public string Location;
		public string Category;
    }

	public struct salesCount
	{
		public int count;
	}

    class SalesController
    {
        private string ConnectionString;

        public SalesController (string connectString)
        {
            this.ConnectionString = connectString;
        }

        public salesRecord[] getSales(int offset, int limit)
        {
            var sql = @"select SalesOrderHeader.SalesOrderNumber, SalesOrderHeader.OrderDate, Store.Name as CompanyName, Address.AddressLine1 as Address, Address.City, 
						Salesman.FirstName + ' ' + Salesman.LastName as SalesRep, StateProvince.StateProvinceCode as [State], SalesOrderDetail.OrderQty, 
						SalesOrderDetail.LineTotal, Product.Name as ProductName,Product.StandardCost as Cost, ProductPhoto.LargePhoto as ProductPhoto, SalesTerritory.[Group] as [Location],
						ProductCategory.Name as Category
						from Sales.SalesOrderDetail
						inner join Sales.SalesOrderHeader on (SalesOrderDetail.SalesOrderID = SalesOrderHeader.SalesOrderID)
						inner join Sales.Customer on (SalesOrderHeader.CustomerID = Customer.CustomerID) 
						inner join Sales.Store on (Customer.StoreID = Store.BusinessEntityID) 
						inner join Person.Address on (SalesOrderHeader.ShipToAddressID = Address.AddressID)
						inner join Person.StateProvince on (Address.StateProvinceID = StateProvince.StateProvinceID)
						inner join Sales.SalesTerritory on (SalesOrderHeader.TerritoryID = SalesTerritory.TerritoryID)
						inner join Production.Product on (SalesOrderDetail.ProductID = Product.ProductID)
						inner join Sales.SalesPerson on (SalesOrderHeader.SalesPersonID = SalesPerson.BusinessEntityID)
						inner join Person.Person as Salesman on (SalesPerson.BusinessEntityID = Salesman.BusinessEntityID)
						inner join Production.ProductProductPhoto on (Product.ProductID = ProductProductPhoto.ProductID)
						inner join Production.ProductPhoto on (ProductProductPhoto.ProductPhotoID = ProductPhoto.ProductPhotoID)
						inner join Production.ProductCategory on (Product.ProductSubcategoryID = ProductCategory.ProductCategoryID)
						order by SalesOrderHeader.OrderDate ";
			sql += "offset " + offset + " rows fetch next " + limit + " rows only ";

            return fillSalesData(sql).ToArray();
        }

		public int getSalesCount()
		{
			var sql = @"select count(*) as Count
						from Sales.SalesOrderDetail
						inner join Sales.SalesOrderHeader on (SalesOrderDetail.SalesOrderID = SalesOrderHeader.SalesOrderID)
						left join Sales.Customer on (SalesOrderHeader.CustomerID = Customer.CustomerID) 
						left join Sales.Store on (Customer.StoreID = Store.BusinessEntityID) 
						left join Person.Address on (SalesOrderHeader.ShipToAddressID = Address.AddressID)
						left join Person.StateProvince on (Address.StateProvinceID = StateProvince.StateProvinceID)
						left join Sales.SalesTerritory on (SalesOrderHeader.TerritoryID = SalesTerritory.TerritoryID)
						left join Production.Product on (SalesOrderDetail.ProductID = Product.ProductID)
						left join Sales.SalesPerson on (SalesOrderHeader.SalesPersonID = SalesPerson.BusinessEntityID)
						left join Person.Person as Salesman on (SalesPerson.BusinessEntityID = Salesman.BusinessEntityID)
						left join Production.ProductCategory on (Product.ProductSubcategoryID = ProductCategory.ProductCategoryID)";
			return fillCount(sql);
		}

        private List<salesRecord> fillSalesData(string salesSql)
        {
            List<salesRecord> sales = new List<salesRecord>();

            var commandText = string.Format(salesSql);
            using (var connection = new SqlConnection(this.ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
						while (reader.Read())
						{
							salesRecord saleRec = new salesRecord();
							saleRec.SalesOrderNumber = reader["SalesOrderNumber"].ToString().Trim();
							saleRec.OrderDate = reader["OrderDate"].ToString().Trim();
							saleRec.CompanyName = reader["CompanyName"].ToString().Trim();
							saleRec.Address = reader["Address"].ToString().Trim();
							saleRec.City = reader["City"].ToString().Trim();
							saleRec.SalesRep = reader["SalesRep"].ToString().Trim();
							saleRec.State = reader["State"].ToString().Trim();
							saleRec.OrderQty = Convert.ToInt32(reader["OrderQty"]);
							saleRec.LineTotal = Convert.ToDouble(reader["LineTotal"]);
							saleRec.ProductName = reader["ProductName"].ToString().Trim();
							saleRec.Cost = Convert.ToDouble(reader["Cost"]);
							saleRec.Location = reader["Location"].ToString().Trim();
							saleRec.Category = reader["Category"].ToString().Trim();

							sales.Add(saleRec);
						}
					}
                }
            }
            return sales;
        }

		private int fillCount(string countSql)
		{
			salesCount count = new salesCount();

			var commandText = string.Format(countSql);
			using (var connection = new SqlConnection(this.ConnectionString))
			{
				connection.Open();
				using (var command = new SqlCommand(commandText, connection))
				{
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							count.count = Convert.ToInt32(reader["Count"]);
						}
					}
				}
			}
			return count.count;
		}
	}
}
