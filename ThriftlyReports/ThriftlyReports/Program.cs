﻿using System;
using System.Configuration;
using Thriftly.Server;
using ThriftlyReports.services;

namespace ThriftlyReports
{

    /// <summary>
    /// Main Program entry point. Create the services and add them to the Thriftly Server instance. 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //Open App.Config and get the connection string. 
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            String connectionSection = "DEVConnectionString";
            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            string dbConn = connectionStringsSection.ConnectionStrings[connectionSection].ConnectionString;

            //init thriftly server
            ThriftlyServer thriftly = new ThriftlyServer();

            // API Services available. Pass the connection string to the service.
            ReportService reportAPI = new ReportService(dbConn);

            try
            {
                thriftly.AddService(reportAPI, "ReportService");
                thriftly.StartServer();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }
    }
}
