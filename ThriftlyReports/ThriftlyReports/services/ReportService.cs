﻿using System;
using Thriftly.Server;
using ThriftlyReports.controllers;

namespace ThriftlyReports.services
{
    /// <summary>
    /// Inherit from security service if a JWT auth method is needed for published functions.
    /// </summary>
    class ReportService : SecurityService
    {

        private SalesController salesController;
        /// <summary>
        /// A connection string needs to be passed to the service class. It will then pass to any controllers that need a connection string to get their respective data.
        /// This service class is meant to expose functions. Underlying logic should be placed in a separate class.
        /// </summary>
        /// <param name="dbConnect"></param>
        public ReportService(string dbConnect)
        {
            //initize one or mo
            this.salesController = new SalesController(dbConnect);

        }

        /// <summary>
        /// Login Method to get a JWT. 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [UnsecuredAttribute(Login = true)]
        public JWT LoginUser(String username, String password, int expiresIn)
        {
            JWT clientJWT = new JWT();

            if (username == "thriftly" && password == "thriftly")
            {
                DateTime now = DateTime.Now;

                clientJWT.iis = "Thriflty.io";
                clientJWT.sub = "tester name";
                clientJWT.email = "test@mertechdata.com";
                clientJWT.isAdmin = true;
                clientJWT.iat = now;
                clientJWT.exp = now.AddDays(expiresIn);
                return clientJWT;
            }

            throw new Exception("Login failed");
        }

		/// <summary>
		/// Secured API Call. Returns the count of all sales records. 
		/// </summary>
		/// <returns></returns>
		[PublishedAttribute]
		public int sales_data_count()
		{
			int count = salesController.getSalesCount();
			return count;
		}

		/// <summary>
		/// Secured API Call. Returned data will be a list of sales records.
		/// </summary>
		/// <param name="sales"></param>
		/// <returns></returns>
		[PublishedAttribute]
        public salesRecord[] sales_data(int offset, int limit)
        {
			var sales = salesController.getSales(offset, limit);
            return sales;
        }
    }  
}
