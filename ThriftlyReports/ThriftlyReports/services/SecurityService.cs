﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thriftly.Server;

namespace ThriftlyReports.services
{
    public struct JWT
    {
        public string iis;
        public string sub;
        public string email;
        public Boolean isAdmin;
        public DateTime iat;
        public DateTime exp;
    }

    class SecurityService
    {
        [AuthAttribute]
        public JWT AuthUser(int methodId, String methodNme, JWT clientJWT)
        {

            DateTime now = DateTime.Now;
            int isExpired = DateTime.Compare(clientJWT.exp, now);
            if (isExpired < 0)
            {
                throw new Exception("JWT is expired");
                
            }
            // one hour expiration
            clientJWT.exp = now.AddHours(1);
            return clientJWT;
            
        }
    }
}
